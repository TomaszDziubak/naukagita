/**
  ******************************************************************************
  * File Name          : main.hpp
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Includes ------------------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define OUT_8_Pin GPIO_PIN_2
#define OUT_8_GPIO_Port GPIOE
#define OUT_7_Pin GPIO_PIN_3
#define OUT_7_GPIO_Port GPIOE
#define migacz_L_OUT_6_Pin GPIO_PIN_4
#define migacz_L_OUT_6_GPIO_Port GPIOE
#define migacz_P_OUT_5_Pin GPIO_PIN_5
#define migacz_P_OUT_5_GPIO_Port GPIOE
#define klakson_OUT_4_Pin GPIO_PIN_6
#define klakson_OUT_4_GPIO_Port GPIOE
#define swiatla_stop_OUT_3_Pin GPIO_PIN_13
#define swiatla_stop_OUT_3_GPIO_Port GPIOC
#define swiatla_mijania_OUT_2_Pin GPIO_PIN_14
#define swiatla_mijania_OUT_2_GPIO_Port GPIOC
#define swiatla_drogowe_OUT_1_Pin GPIO_PIN_15
#define swiatla_drogowe_OUT_1_GPIO_Port GPIOC
#define PH0_OSC_IN_Pin GPIO_PIN_0
#define PH0_OSC_IN_GPIO_Port GPIOH
#define PH1_OSC_OUT_Pin GPIO_PIN_1
#define PH1_OSC_OUT_GPIO_Port GPIOH
#define GPS___UART4_TX_Pin GPIO_PIN_0
#define GPS___UART4_TX_GPIO_Port GPIOA
#define GPS___UART4_RX_Pin GPIO_PIN_1
#define GPS___UART4_RX_GPIO_Port GPIOA
#define DAC_OUT2_Throttle_out_Pin GPIO_PIN_5
#define DAC_OUT2_Throttle_out_GPIO_Port GPIOA
#define IN_12_Pin GPIO_PIN_5
#define IN_12_GPIO_Port GPIOC
#define IN_11_Pin GPIO_PIN_0
#define IN_11_GPIO_Port GPIOB
#define IN_10_Pin GPIO_PIN_1
#define IN_10_GPIO_Port GPIOB
#define BOOT1_Pin GPIO_PIN_2
#define BOOT1_GPIO_Port GPIOB
#define IN_9_Pin GPIO_PIN_7
#define IN_9_GPIO_Port GPIOE
#define IN_8_Pin GPIO_PIN_8
#define IN_8_GPIO_Port GPIOE
#define IN_7_Pin GPIO_PIN_9
#define IN_7_GPIO_Port GPIOE
#define IN_6_Pin GPIO_PIN_10
#define IN_6_GPIO_Port GPIOE
#define IN_5_Pin GPIO_PIN_11
#define IN_5_GPIO_Port GPIOE
#define IN_4_Pin GPIO_PIN_12
#define IN_4_GPIO_Port GPIOE
#define IN_3_Pin GPIO_PIN_13
#define IN_3_GPIO_Port GPIOE
#define IN_2_Pin GPIO_PIN_14
#define IN_2_GPIO_Port GPIOE
#define IN_1_Pin GPIO_PIN_15
#define IN_1_GPIO_Port GPIOE
#define USART3_TX_Pin GPIO_PIN_8
#define USART3_TX_GPIO_Port GPIOD
#define USART3_RX_Pin GPIO_PIN_9
#define USART3_RX_GPIO_Port GPIOD
#define USART3_CK_Pin GPIO_PIN_10
#define USART3_CK_GPIO_Port GPIOD
#define PWM4_T1_Pin GPIO_PIN_12
#define PWM4_T1_GPIO_Port GPIOD
#define PWM3_T2_Pin GPIO_PIN_13
#define PWM3_T2_GPIO_Port GPIOD
#define PWM2_Pin GPIO_PIN_14
#define PWM2_GPIO_Port GPIOD
#define PWM1_Pin GPIO_PIN_15
#define PWM1_GPIO_Port GPIOD
#define OUT_12_T3_Pin GPIO_PIN_6
#define OUT_12_T3_GPIO_Port GPIOC
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define OUT_9_Pin GPIO_PIN_2
#define OUT_9_GPIO_Port GPIOD
#define OUT_10_Pin GPIO_PIN_3
#define OUT_10_GPIO_Port GPIOD
#define OUT_11_Pin GPIO_PIN_6
#define OUT_11_GPIO_Port GPIOD
#define TIM3_CH1_pomiar_predkosci_Pin GPIO_PIN_4
#define TIM3_CH1_pomiar_predkosci_GPIO_Port GPIOB
#define TIM3_CH2_pomiar_predkosci_Pin GPIO_PIN_5
#define TIM3_CH2_pomiar_predkosci_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
